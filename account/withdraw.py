import pymysql.cursors
from module.conn import Connection
from account.norek import NomorRekening
from account.saldo import Saldo
import datetime

class Withdraw:

    def __init__(self):
        con = Connection()
        self.connection = con.get()

    def Take(self, norek, amount):
        cursor = self.connection.cursor()

        if amount > 0:
            #Get saldo nasabah
            data = Saldo()
            data_saldo = data.Check(norek)
            
            #Validasi jika uang lebih dari jumlah
            saldo = data_saldo[2]['saldo']
            if saldo >= amount:
                #Saldo rekening dikurang jumlah penarikan
                saldo -= amount
                #Validasi nomor rekening
                rekening = NomorRekening()
                if rekening.isExists(norek):
                    temp_date = datetime.datetime.now()
                    date = temp_date.strftime("%Y-%m-%d %H:%M")

                    cursor.execute('INSERT INTO withdraw (norek, saldo, date) values(%s, %s, %s)', (norek, amount, date))
                    self.connection.commit()

                    #Update saldo
                    data.Update(norek, saldo)

                    return 'Success', 'Successfully withdraw'
                else:
                    return 'Failed', 'Rekening number is wrong'
            else:
                return 'Failed', 'Your money is not enough'
        else:
            return 'Failed', 'Money cant less than zero'