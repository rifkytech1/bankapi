import pymysql.cursors
from module.conn import Connection

class NomorRekening:

    def __init__(self):
        con = Connection()
        self.connection = con.get()

    def isExists(self, norek):
        sql = 'SELECT * FROM nasabah WHERE norek = %s'
        cursor = self.connection.cursor()

        #Execute
        cursor.execute(sql, norek)
        data = cursor.fetchone()
        
        if data == None:
            return False
        else:
            return True