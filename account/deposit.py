import pymysql.cursors
from module.conn import Connection
from account.norek import NomorRekening
from account.saldo import Saldo
import datetime

class Deposit:

    def __init__(self):
        con = Connection()
        self.connection = con.get()

    def Insert(self, norek, amount):
        cursor = self.connection.cursor()

        if amount > 0:
            #Execute
            temp_date = datetime.datetime.now()
            date = temp_date.strftime("%Y-%m-%d %H:%M")

            #Validasi nomor rekening
            rekening = NomorRekening()
            if rekening.isExists(norek):
                cursor.execute('INSERT INTO deposit(norek, saldo, date) values(%s, %s, %s)', (norek, amount, date))
                self.connection.commit()

                #Get saldo nasabah
                data = Saldo()
                data_saldo = data.Check(norek)

                saldo = data_saldo[2]['saldo']
                saldo += amount
                data.Update(norek, saldo)

                return 'Success', 'Successfully deposit'
            else:
                return 'Failed', 'Rekening Number is wrong'
        else:
            return 'Failed', 'Money cant less than zero'