from account.withdraw import Withdraw
from account.transfer import Transfer
from account.deposit import Deposit
from module.keys import Keys

class Transaction_Controller:

    def __init__(self, apikey):
        self.apikey = apikey

    '''

    Function ini berfungsi untuk melakukan proses
    withdraw, yaitu penarikan saldo dari rekening nasabah

    '''

    def Take(self, norek, amount):
        withdraw = Withdraw()

        keys = Keys()
        if keys.isExists(self.apikey):
            status, messages = withdraw.Take(norek, amount)
        else:
            status = 'Failed'
            messages = 'Apikey is invalid'

        data = {
            'status_info': status,
            'status_messages': messages
        }

        return data

    '''

    Function ini berfungsi untuk melakukan proses deposit,
    yaitu setoran saldo ke rekening nasabah

    '''
    def Insert(self, norek, amount):
        deposit = Deposit()

        keys = Keys()
        if keys.isExists(self.apikey):
            status, messages = deposit.Insert(norek, amount)
        else:
            status = 'Failed'
            messages = 'Apikey is invalid'
        
        data = {
            'status_info': status,
            'status_messages': messages
        }

        return data

    '''

    Function ini berfungsi untuk melakukan transfer saldo
    antara nasabah. Diperlukan nomor rekening pengirim
    dan nomor rekening penerima

    '''
    def Send(self, from_norek, to_norek, amount, notes):
        transfer = Transfer()

        keys = Keys()
        if keys.isExists(self.apikey):
            status, messages = transfer.Send(from_norek, to_norek, amount, notes)
        else:
            status = 'Failed'
            messages = 'Apikey is invalid'
        
        data = {
            'status_info': status,
            'status_messages': messages
        }

        return data