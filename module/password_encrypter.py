from module.crypter import Crypter

class Password:

    def __init__(self, password, secret_key):
        self.password = password
        self.secret_key = secret_key

    def Aesis(self):
        if self.secret_key == '1129874678923456':
            #Set password length to 16 bytes
            while(len(self.password) < 16):
                self.password += "0o"

            #Mari kita enkripsi passwordnya
            crypter = Crypter(self.password, self.secret_key)
            enc_password = crypter.Encrypt()
            enc_password = enc_password.replace(b'++', b'Y0')

            return enc_password